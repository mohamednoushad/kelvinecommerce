pragma solidity ^0.5.1;


contract magicMall {
    
      modifier onlyAdministrator(){
        address _customerAddress = msg.sender;
        require(administrators[_customerAddress]);
        _;
    }
    
      event onTokenPurchase(
        address indexed customerAddress,
        uint256 incomingEthereum,
        uint256 tokensMinted
    );
    
    // ERC20
    event Transfer(
        address indexed from,
        address indexed to,
        uint256 tokens
    );
    
     /*=====================================
    =            CONFIGURABLES            =
    =====================================*/
    string public name = "MagicT";
    string public symbol = "MGT";
    uint8 constant public decimals = 2;
    uint public taiwanCurrenciesFor1Eth = 6100;
    
    //has to come under token price -- variable function
    uint256 tokenPrice = SafeMath.div(1, taiwanCurrenciesFor1Eth);
    uint256 _tokenPrice = tokenPrice * 1e18;
    
    uint256 internal tokenSupply_ = 0;
    uint256 internal profitPerShare_;
    uint256 public tokenDistributionPool;
    
    
    mapping(address => bool) public administrators;
    
     /*================================
    =            DATASETS            =
    ================================*/
    // amount of shares for each address (scaled number)
    mapping(address => uint256) public tokenBalanceLedger_;
    
    
    constructor() public {
        administrators[0x3C0B8Cb523304079e76132544c282a3D86Ebc0F7] = true;
    }
    
    
    function buy() public payable returns(uint256) {
         purchaseTokens(msg.value);
    }
    
    function() payable external {
        purchaseTokens(msg.value);
    }
      
    function purchaseTokens(uint256 _incomingEthereum) internal returns(uint256)   {
       
        address _customerAddress = msg.sender;
        uint256 _amountOfTokens = ethereumToTokens_(_incomingEthereum);
     
        require(_amountOfTokens > 0 && (SafeMath.add(_amountOfTokens,tokenSupply_) > tokenSupply_));
        
        //adding equivalent tokens to other users redemption pool
        tokenDistributionPool = SafeMath.add(tokenDistributionPool,_amountOfTokens);
        
        // we can't give people infinite tokens
        if(tokenSupply_ > 0){
            
            // add tokens to the main pool
            tokenSupply_ = SafeMath.add(tokenSupply_, _amountOfTokens);
 
            // take the amount of dividends gained through this transaction, and allocates them evenly to each shareholder
            profitPerShare_ += (_amountOfTokens / (tokenSupply_));
            
 
        } else {
            // add tokens to the pool
            tokenSupply_ = _amountOfTokens;
        }
        
        // update circulating supply & the ledger address for the customer
        tokenBalanceLedger_[_customerAddress] = SafeMath.add(tokenBalanceLedger_[_customerAddress], _amountOfTokens);
        
         // fire event
        emit onTokenPurchase(_customerAddress, _incomingEthereum, _amountOfTokens);
        
        return _amountOfTokens;
    }
    
    
    function ethereumToTokens_(uint256 _ethereum) internal view returns(uint256) {
       
        uint256 _tokensReceived = taiwanCurrenciesFor1Eth * _ethereum *  1e18    ;
  
        return _tokensReceived;
    }
  

    

    
 
}

/**
 * @title SafeMath
 * @dev Math operations with safety checks that throw on error
 */
library SafeMath {

    /**
    * @dev Multiplies two numbers, throws on overflow.
    */
    function mul(uint256 a, uint256 b) internal pure returns (uint256) {
        if (a == 0) {
            return 0;
        }
        uint256 c = a * b;
        assert(c / a == b);
        return c;
    }

    /**
    * @dev Integer division of two numbers, truncating the quotient.
    */
    function div(uint256 a, uint256 b) internal pure returns (uint256) {
        // assert(b > 0); // Solidity automatically throws when dividing by 0
        uint256 c = a / b;
        // assert(a == b * c + a % b); // There is no case in which this doesn't hold
        return c;
    }

    /**
    * @dev Substracts two numbers, throws on overflow (i.e. if subtrahend is greater than minuend).
    */
    function sub(uint256 a, uint256 b) internal pure returns (uint256) {
        assert(b <= a);
        return a - b;
    }


    /**
    * @dev Adds two numbers, throws on overflow.
    */
    function add(uint256 a, uint256 b) internal pure returns (uint256) {
        uint256 c = a + b;
        assert(c >= a);
        return c;
    }
}
