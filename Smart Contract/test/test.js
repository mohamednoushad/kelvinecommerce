let tokensOfUser = {};
let totalSupply = 0;
let poolTokens = 0;
let userCountArray = [];
let intialStageOfUserBuying = {};
let ethInTheStage = {};
let totalEth = 0;
let tokensFor1Eth = 12000;

async function estimateUserDividend(stage, usernumber, incomingEth) {

    let tokensForUser = incomingEth * (tokensFor1Eth / 2);

    tokensOfUser[usernumber] = tokensForUser;
    poolTokens = poolTokens + tokensForUser;
    totalSupply = totalSupply + 2 * tokensForUser;
    userCountArray.push(usernumber);

    intialStageOfUserBuying[usernumber] = stage;
    totalEth = totalEth + incomingEth;
    ethInTheStage[stage] = totalEth;

    let userRealPoolDividend = totalEth / 2;
    let adminsPool = totalEth / 2;

    let addedUpUserPooldividend = 0;

    for (const i in userCountArray) {

        var usernumber = userCountArray[i];
        var usersToken = tokensOfUser[usernumber];
        var stageNo = usernumber;
        let div = (usersToken / (totalSupply / 2)) * (totalEth - ethInTheStage[stageNo]);
        addedUpUserPooldividend = addedUpUserPooldividend + div;
        console.log(usernumber + ' th/st/nd user dividend is ' + div);

    }
    console.log(userRealPoolDividend + " is the real user pool divident while the pool consumed by users in this stage is " + addedUpUserPooldividend);
    console.log("Eth for Dr Kelvin (50%)", 0.5 * adminsPool);
    console.log("Eth for Kelvins First Friend (25%)", 0.25 * adminsPool);
    console.log("Eth for Kelvins Second Friend (25%)", 0.25 * adminsPool);



}

async function iterateForUsers() {

    const stageArray = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20];

    for (const i in stageArray) {
        console.log('=====starting stage======', stageArray[i])
        await estimateUserDividend(stageArray[i], stageArray[i], 1);
    }

}

iterateForUsers();

// estimateUserDividend(1, 1, 1);
// estimateUserDividend(2, 2, 1);