/* JS Document */

/******************************

[Table of Contents]

1. Vars and Inits
2. Init Menu


******************************/

$(document).ready(function()
{
    
    var arrLang = {
        'en': {
            'home' : 'Home',
            'woman' : 'About Us',
            'man' : 'Man',
            'lookbook' : 'Lookbook',
            'blog' : 'Blog',
            'contact': 'Contact'
        },
        'ch': {
            'home' : 'Chinese',
            'woman' : 'Chinese Us',
            'man' : 'Chinese',
            'lookbook' : 'Chinese',
            'blog' : 'Chinese',
            'contact': 'Chinese'

        }
    }

    $('.language_flag').click(function() {

        var lang = $(this).attr('id');

        $('.lang').each(function(index,element){
            $(this).text(arrLang[lang][$(this).attr('key')]);
        })
    })

    $('.language_text').click(function() {
 
        var lang = $(this).attr('id');
        var realLang;

        if(lang=="en2"){
            realLang = "en";
        } else if (lang=="ch2") {
            realLang = "ch";
        }

        $('.lang').each(function(index,element){
            $(this).text(arrLang[realLang][$(this).attr('key')]);
        })
    })

});