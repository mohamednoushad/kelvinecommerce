const express = require('express');
const common = require('../common');
const {
    indexOrders
} = require('../indexing');
const numeral = require('numeral');
const stripe = require('stripe')(common.getPaymentConfig().secretKey);
const router = express.Router();

// The homepage of the site
router.post('/checkout_action', async (req, res, next) => {

    console.log("calling metamask checkout");

    try {



        const db = req.app.db;
        const config = req.app.config;
        const metaMaskConfig = common.getPaymentConfig();

        console.log(req.body);

        let data = req.body;

        // { payment: 'metamask',
        // paymentHash: '0x64a2bc2e94f62982d4b20fa7ebc224662507c6cbb8ae89f043bfd11aa39cc7ac' }
        let response;

        let paymentStatus = 'Paid';


        // new order doc
        const orderDoc = {
            orderPaymentId: data.paymentHash,
            orderPaymentGateway: 'Metamask',
            orderPaymentMessage: 'success',
            orderTotal: req.session.totalCartAmount,
            orderShipping: req.session.totalCartShipping,
            orderItemCount: req.session.totalCartItems,
            orderProductCount: req.session.totalCartProducts,
            orderCustomer: common.getId(req.session.customerId),
            orderEmail: req.session.customerEmail,
            orderCompany: req.session.customerCompany,
            orderFirstname: req.session.customerFirstname,
            orderLastname: req.session.customerLastname,
            orderAddr1: req.session.customerAddress1,
            orderAddr2: req.session.customerAddress2,
            orderCountry: req.session.customerCountry,
            orderState: req.session.customerState,
            orderPostcode: req.session.customerPostcode,
            orderPhoneNumber: req.session.customerPhone,
            orderComment: req.session.orderComment,
            orderStatus: paymentStatus,
            orderDate: new Date(),
            orderProducts: req.session.cart,
            orderType: 'Single'
        };

        console.log(orderDoc);

        // insert order into DB
        const newOrder = await db.orders.insertOne(orderDoc);

        // get the new ID
        const newId = newOrder.insertedId;

        // add to lunr index
        indexOrders(req.app)
            .then(() => {
                // Process the result
                if (paymentStatus === 'Paid') {
                    // set the results
                    req.session.messageType = 'success';
                    req.session.message = 'Your payment was successfully completed';
                    req.session.paymentEmailAddr = orderDoc.orderEmail;
                    req.session.paymentApproved = true;
                    req.session.paymentDetails = '<p><strong>Order ID: </strong>' + newId + '</p><p><strong>Transaction ID: </strong>' + data.paymentHash + '</p>';

                    // set payment results for email
                    const paymentResults = {
                        message: req.session.message,
                        messageType: req.session.messageType,
                        paymentEmailAddr: req.session.paymentEmailAddr,
                        paymentApproved: true,
                        paymentDetails: req.session.paymentDetails
                    };

                    // clear the cart
                    if (req.session.cart) {
                        common.emptyCart(req, res, 'function');
                    }

                    // send the email with the response
                    // TODO: Should fix this to properly handle result
                    common.sendEmail(req.session.paymentEmailAddr, 'Your payment with ' + config.cartTitle, common.getEmailTemplate(paymentResults));
                }
                res.status(200).json({
                    paymentId: newId
                });
            });

    } catch (error) {
        console.log(error);

    }

});




module.exports = router;