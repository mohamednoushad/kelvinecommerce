console.log("hello world");


console.log("hello");

let tokensFor1Eth;

const App = {
    web3: null,
    account: null,
    meta: null,

    start: async function () {
        const {
            web3
        } = this;

        console.log("App Started");

        try {
            console.log(web3.eth.accounts);


            // get contract instance
            const networkId = await web3.eth.net.getId();
            console.log(networkId);
            //   const deployedNetwork = autoMobileArtifact.networks[networkId];
            //   this.meta = new web3.eth.Contract(
            //     autoMobileArtifact.abi,
            //     deployedNetwork.address,
            //   );

            // get accounts
            const accounts = await web3.eth.getAccounts();
            this.account = accounts[0];
            console.log(this.account);
            if (this.account) {
                $('#userAddress').html(this.account);
            } else {
                $('#userAddress').html("cannot be obtained. Please connect Metamask");
            }


            //   console.log(this.account);

            this.refreshTable();
        } catch (error) {
            $('#userAddress').html("cannot be obtained. Please connect Metamask");
            console.error("Could not connect to contract or chain.");
        }
    },

    refreshTable: async function () {

        // const {
        //   getLatestCarRegisteredNumber
        // } = this.meta.methods;
        // const totalCars = await getLatestCarRegisteredNumber().call();
        let userTokenBalanceFromContract = 0;

        $('#userTokenBalance').html(userTokenBalanceFromContract);

        tokensFor1Eth = 6000.00;

        $('#tokensForOneEther').html(tokensFor1Eth);




    }


}


window.App = App;

$(document).ready(function () {

    if (window.ethereum) {
        // use MetaMask's provider
        App.web3 = new Web3(window.ethereum);
        `  `
        window.ethereum.enable(); // get permission to access accounts
        console.log("using metamask");
        // console.log(web3.eth.accounts);
        // window.ethereum.on('accountsChanged', function (accounts) {
        //   location.reload(); 
        // })
        // window.ethereum.on('networkChanged', function (accounts) {
        //   location.reload(); 
        // })
    } else {
        console.warn(
            "No web3 detected. Falling back to http://127.0.0.1:8545. You should remove this fallback when you deploy live",
        );
        // fallback - use your fallback strategy (local node / hosted node + in-dapp id mgmt / fail)
        App.web3 = new Web3(
            new Web3.providers.HttpProvider("http://127.0.0.1:8545"),
        );
    }

    App.start();

    $("#tokensToEstimateEther").keyup(function () {

        let userInputTokensForEstimate = $("#tokensToEstimateEther").val();
        console.log(tokensFor1Eth);
        console.log(userInputTokensForEstimate);
        var approximateEtherForTokens = +(userInputTokensForEstimate) / +(tokensFor1Eth);
        $('#etherForInputTokens').html(approximateEtherForTokens);

    });

    $("#ethersToBuyToken").keyup(function () {

        let userInputEtherForEstimate = $("#ethersToBuyToken").val();
        console.log(tokensFor1Eth);
        console.log(userInputEtherForEstimate);
        var approximateEtherForTokens = +(userInputEtherForEstimate) * +(tokensFor1Eth);
        $('#approximateTokensForInputEther').html(approximateEtherForTokens.toFixed(2));

    });



});