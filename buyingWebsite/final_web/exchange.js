var dataTimer = null;
let currentAddress;
var dividendValue = 0;
var tokenBalance = 0;
let actual_JSON;
var ethPrice = 0;
var ethPriceTimer = null;
let ethTaiwanPrice = 0;
let ethTaiwanPriceTimer = null;
var currency = (typeof default_currency === 'undefined') ? 'USD' : default_currency;
var buyPrice = 0;
var globalBuyPrice = 0;
let contract;
var sellPrice = 0;
var infoTimer = null;
let masterNodeLink;



function convertWeiToEth(e) {
  return e / 1e18
}


function convertEthToWei(e) {
  return 1e18 * e
}




// function loadJSON(callback) {

//   var xobj = new XMLHttpRequest();
//   xobj.overrideMimeType("application/json");
//   xobj.open('GET', 'mmtToken.json', true);
//   xobj.onreadystatechange = function () {
//     if (xobj.readyState == 4 && xobj.status == "200") {
//       // Required use of an anonymous callback as .open will NOT return a value but simply returns undefined in asynchronous mode
//       callback(xobj.responseText);
//     }
//   };
//   xobj.send(null);
// }



function getCookie(name) {
  var dc = document.cookie;
  var prefix = name + "=";
  var begin = dc.indexOf("; " + prefix);

  if (begin == -1) {
    begin = dc.indexOf(prefix);
    if (begin != 0) return null;
  } else {
    begin += 2;
    var end = document.cookie.indexOf(";", begin);
    if (end == -1) {
      end = dc.length;
    }
  }

  return decodeURI(dc.substring(begin + prefix.length, end));
}



function whenAvailable(name, callback) {
  var interval = 6000;
  window.setTimeout(function () {
    if (window[name]) {
      callback(window[name]);
    } else {
      window.setTimeout(arguments.callee, interval);
    }
  }, interval);
}




// function updateData() {
//   // clearTimeout(dataTimer)

//   web3.eth.getBalance(contractAddress, function (e, r) {
//     $('.contract-balance').text(convertWeiToEth(r).toFixed(4) + " ETH")
//     $('.contract-balance-usd').text('(' + Number((convertWeiToEth(r) * ethPrice).toFixed(2)).toLocaleString() + ' ' + currency + ')');
//   })

//   contract.totalSupply(function (e, r) {
//     let actualSupply = r / 1e18;
//     $('.contract-tokens').text(Number(actualSupply.toFixed(0)).toLocaleString());
//   })

//   contract.buyPrice(function (e, r) {
//     let buyPrice = convertWeiToEth(r)
//     globalBuyPrice = convertWeiToEth(r)
//     $('.buy').text(buyPrice.toFixed(6) + ' ETH')
//     $('.buy-usd').text('(' + Number((buyPrice * ethPrice).toFixed(2)).toLocaleString() + ' ' + currency + ')')
//   })

//   contract.sellPrice(function (e, r) {
//     let sellPrice = convertWeiToEth(r)
//     $('.sell').text(sellPrice.toFixed(6) + ' ETH')
//     $('.sell-usd').text('(' + Number((sellPrice * ethPrice).toFixed(2)).toLocaleString() + ' ' + currency + ')')
//   })

//   if (currentAddress) {
//     contract.balanceOf(currentAddress, function (e, r) {
//       const tokenAmount = (r / 1e18 * 0.9999)
//       $('.balance').text(Number(tokenAmount.toFixed(2)).toLocaleString())
//       contract.calculateEthereumReceived(r, function (e, r) {
//         let bal = convertWeiToEth(r)
//         $('.value').text(bal.toFixed(4))
//         $('.value-usd').text(Number((convertWeiToEth(r * 1) * ethPrice).toFixed(2)).toLocaleString())
//         if (tokenBalance !== 0) {
//           if (bal > tokenBalance) {
//             $('.value').addClass('up').removeClass('down')
//             setTimeout(function () {
//               $('.value').removeClass('up')
//             }, 3000)
//           } else if (bal < tokenBalance) {
//             $('.value').addClass('down').removeClass('up')
//             setTimeout(function () {
//               $('.value').removeClass('down')
//             }, 3000)
//           }
//         }
//         tokenBalance = bal
//       })
//     })
//   }

//   contract.myDividends(true, function (e, r) {
//     let div = convertWeiToEth(r).toFixed(6)

//     $('.div').text(div)
//     $('input.div').val(div + " ETH")
//     $('.div-usd').text(Number((convertWeiToEth(r) * ethPrice).toFixed(2)).toLocaleString())

//     if (dividendValue != div) {
//       $('.div').fadeTo(100, 0.3, function () {
//         $(this).fadeTo(250, 1.0)
//       })

//       dividendValue = div
//     }

//     contract.myDividends(false, function (e, r) {
//       let div = convertWeiToEth(r).toFixed(6)
//       let refdiv = (dividendValue - div).toFixed(6);

//       $('.refdiv').text(refdiv)
//       $('.refdiv-usd').text(Number((refdiv * ethPrice).toFixed(2)).toLocaleString())

//       $('.nonrefdiv').text(div)
//       $('.nonrefdiv-usd').text(Number((convertWeiToEth(r) * ethPrice).toFixed(2)).toLocaleString())
//     })
//   })

//   contract.stakingRequirement(function (e, r) {
//     $('#tokenRequirement').text(convertWeiToEth(r));
//   })

//   contract.dividendFee_(function (e, r) {
//     let divDisplayValue = r;

//     //    switch (r.toNumber()) {
//     //     case 10:
//     //         divDisplayValue = 10;
//     //         break;
//     //     case 5:
//     //         divDisplayValue = 20;
//     //         break;
//     //     case 4:
//     //         divDisplayValue = 25;
//     //         break;
//     //     case 3:
//     //         divDisplayValue = 33;
//     //         break;
//     //      default:
//     //         break;
//     // }
//     $('#divPercentage').text(divDisplayValue);
//     $('#divPercentageSecond').text(divDisplayValue);
//     $('#divPercentageThird').text(divDisplayValue);
//     $('#divPercentageFour').text(divDisplayValue);
//     $('#divPercentageFive').text(divDisplayValue);
//     $('#div-hint').text("Present Divident Percentage is " + divDisplayValue);

//   })

//   contract.onlyAmbassadors(function (e, r) {
//     let div;

//     if (r) {
//       div = 'Enabled';
//     } else if (!r) {
//       div = 'Disabled';
//     }

//     $('input.amb').val("Ambassador Phase is " + div)

//   })

//   contract.refPercentage(function (e, r) {
//     let percentage = r;
//     $('#masterNode-hint').text("The Present Masternode Percentage is " + percentage.toFixed(0) + " !");
//     $('#masterNodeExchange-hint').text(percentage.toFixed(0));
//   })



// }






$('#sell-tokens-amount').on('input change', function () {
  var value = parseFloat($(this).val());


  if (value === 0 || Number.isNaN(value)) {
    $('#sell-hint').text("");
    return;
  }

  if (value > 0) {
    contract.calculateEthereumReceived(convertEthToWei(value), function (e, r) {
      let eth = convertWeiToEth(r);
      $('#sell-hint').text("You will get approximately " + eth.toFixed(2) + " eth!");
    })
  }

});

//appends an "active" class to .popup and .popup-content when the "Open" button is clicked
// $(".open").on("click", function(){
//   $(".popup-overlay, .popup-content").addClass("active");
// });

//removes the "active" class to .popup and .popup-content when the "Close" button is clicked 
$(".close, .popup-overlay").on("click", function () {
  $(".popup-overlay, .popup-content").removeClass("active");
});



// Reinvest click handler
$('#reinvest-btn').click(function () {
  bundleReInvest();
})

// Withdraw click handler
// $('#withdraw-btn').click(function () {
//   bundleWithdraw()
// })

// $('#sell-tokens-btn').click(function () {
//   bundleSell(convertEthToWei($("#sell-tokens-amount").val()))
// })

// $('#transfer-tokens-btn').click(function () {
//   let address = $('#transfer-address').val();
//   let amount = $('#transfer-tokens').val();

//   if (web3.isAddress(address) && parseFloat(amount)) {
//     var amountConverted = convertEthToWei(amount);
//     bundleTransferTokens(address, amountConverted);
//   } else {
//     console("Invalid Address / Amount");
//   }


// });

function openEtherscan() {
  window.open("https://etherscan.io/address/" + contractAddress, "_blank");
}

// function openMasterNodeLink() {
//   window.open("https://"+masterNodeLink,"_blank");
// }



// 

$('#disableAdmin-btn').click(function () {
  disableInitial();
});

function setDivPercentage() {
  event.preventDefault();
  let selectedValue = $("#divPercentages option:selected").val();
  let inputPercentage = parseInt(selectedValue);
  if ((inputPercentage / 10) < 10 && Number.isInteger(inputPercentage) && Number.isInteger(inputPercentage / 10)) {
    setDividentPercentage(inputPercentage);

  }
}


function setMasterNode() {
  event.preventDefault();
  let selectedValue = $("#percentage option:selected").val();
  let inputPercentage = parseInt(selectedValue);

  if ((inputPercentage / 10) < 10 && Number.isInteger(inputPercentage) && Number.isInteger(inputPercentage / 10)) {

    updateMasternodePercentage(inputPercentage);

  }

}




// $(document).ready(function () {
//   // clearTimeout(dataTimer);
//   if (typeof web3 !== 'undefined') {
//     console.warn("Using web3 detected from external source like Metamask")
//     window.web3 = new Web3(web3.currentProvider);
//   } else {
//     console.warn("No web3 detected. Falling back to http://localhost:8545. You should remove this fallback when you deploy live, as it's inherently insecure. Consider switching to Metamask for development. More info here: http://truffleframework.com/tutorials/truffle-and-metamask");
//     window.web3 = new Web3(new Web3.providers.HttpProvider("http://localhost:8545"));

//   }






//   whenAvailable("web3", function () {
//     if (web3.eth.accounts[0] !== null) {
//       //   masterNodeLink = window.location.hostname+"/exchange.html?masternode=" + currentAddress;
//       //   var element = "<br/><a>"+masterNodeLink +"</a>";
//       //  $("#quoteDisplay").append(element);

//     }



//   });

//   dataTimer = setTimeout(function () {
//     updateData()
//   }, 2000)

//   setInterval(updateData, 8000);

// })

const abi = [{
  "constant": false,
  "inputs": [],
  "name": "buy",
  "outputs": [{
    "name": "",
    "type": "uint256"
  }],
  "payable": true,
  "stateMutability": "payable",
  "type": "function"
}, {
  "constant": false,
  "inputs": [{
    "name": "_newTokenAmount",
    "type": "uint256"
  }],
  "name": "changeTokensForOneEther",
  "outputs": [],
  "payable": false,
  "stateMutability": "nonpayable",
  "type": "function"
}, {
  "constant": false,
  "inputs": [],
  "name": "ownerAndFriendsWithdrawDividends",
  "outputs": [],
  "payable": false,
  "stateMutability": "nonpayable",
  "type": "function"
}, {
  "constant": false,
  "inputs": [{
    "name": "_identifier",
    "type": "address"
  }, {
    "name": "_status",
    "type": "bool"
  }],
  "name": "setAdministrator",
  "outputs": [],
  "payable": false,
  "stateMutability": "nonpayable",
  "type": "function"
}, {
  "constant": false,
  "inputs": [{
    "name": "_name",
    "type": "string"
  }],
  "name": "setName",
  "outputs": [],
  "payable": false,
  "stateMutability": "nonpayable",
  "type": "function"
}, {
  "constant": false,
  "inputs": [{
    "name": "_amountOfTokens",
    "type": "uint256"
  }],
  "name": "setStakingRequirement",
  "outputs": [],
  "payable": false,
  "stateMutability": "nonpayable",
  "type": "function"
}, {
  "constant": false,
  "inputs": [{
    "name": "_symbol",
    "type": "string"
  }],
  "name": "setSymbol",
  "outputs": [],
  "payable": false,
  "stateMutability": "nonpayable",
  "type": "function"
}, {
  "constant": false,
  "inputs": [{
    "name": "_toAddress",
    "type": "address"
  }, {
    "name": "_amountOfTokens",
    "type": "uint256"
  }],
  "name": "transfer",
  "outputs": [{
    "name": "",
    "type": "bool"
  }],
  "payable": false,
  "stateMutability": "nonpayable",
  "type": "function"
}, {
  "constant": false,
  "inputs": [],
  "name": "withdraw",
  "outputs": [],
  "payable": false,
  "stateMutability": "nonpayable",
  "type": "function"
}, {
  "inputs": [],
  "payable": false,
  "stateMutability": "nonpayable",
  "type": "constructor"
}, {
  "payable": true,
  "stateMutability": "payable",
  "type": "fallback"
}, {
  "anonymous": false,
  "inputs": [{
    "indexed": true,
    "name": "customerAddress",
    "type": "address"
  }, {
    "indexed": false,
    "name": "incomingEthereum",
    "type": "uint256"
  }, {
    "indexed": false,
    "name": "tokensMinted",
    "type": "uint256"
  }],
  "name": "onTokenPurchase",
  "type": "event"
}, {
  "anonymous": false,
  "inputs": [{
    "indexed": true,
    "name": "customerAddress",
    "type": "address"
  }, {
    "indexed": false,
    "name": "tokensBurned",
    "type": "uint256"
  }, {
    "indexed": false,
    "name": "ethereumEarned",
    "type": "uint256"
  }],
  "name": "onTokenSell",
  "type": "event"
}, {
  "anonymous": false,
  "inputs": [{
    "indexed": true,
    "name": "customerAddress",
    "type": "address"
  }, {
    "indexed": false,
    "name": "ethereumWithdrawn",
    "type": "uint256"
  }],
  "name": "onWithdraw",
  "type": "event"
}, {
  "anonymous": false,
  "inputs": [{
    "indexed": true,
    "name": "from",
    "type": "address"
  }, {
    "indexed": true,
    "name": "to",
    "type": "address"
  }, {
    "indexed": false,
    "name": "tokens",
    "type": "uint256"
  }],
  "name": "Transfer",
  "type": "event"
}, {
  "constant": true,
  "inputs": [{
    "name": "",
    "type": "address"
  }],
  "name": "administrators",
  "outputs": [{
    "name": "",
    "type": "bool"
  }],
  "payable": false,
  "stateMutability": "view",
  "type": "function"
}, {
  "constant": true,
  "inputs": [{
    "name": "",
    "type": "address"
  }],
  "name": "adminsAlreadyConvertedTokensToEth",
  "outputs": [{
    "name": "",
    "type": "uint256"
  }],
  "payable": false,
  "stateMutability": "view",
  "type": "function"
}, {
  "constant": true,
  "inputs": [{
    "name": "_customerAddress",
    "type": "address"
  }],
  "name": "balanceOf",
  "outputs": [{
    "name": "",
    "type": "uint256"
  }],
  "payable": false,
  "stateMutability": "view",
  "type": "function"
}, {
  "constant": true,
  "inputs": [{
    "name": "_ethereumToSpend",
    "type": "uint256"
  }],
  "name": "calculateTokensReceived",
  "outputs": [{
    "name": "",
    "type": "uint256"
  }],
  "payable": false,
  "stateMutability": "view",
  "type": "function"
}, {
  "constant": true,
  "inputs": [],
  "name": "decimals",
  "outputs": [{
    "name": "",
    "type": "uint8"
  }],
  "payable": false,
  "stateMutability": "view",
  "type": "function"
}, {
  "constant": true,
  "inputs": [{
    "name": "",
    "type": "uint256"
  }],
  "name": "ethInTheStage",
  "outputs": [{
    "name": "",
    "type": "uint256"
  }],
  "payable": false,
  "stateMutability": "view",
  "type": "function"
}, {
  "constant": true,
  "inputs": [{
    "name": "",
    "type": "address"
  }],
  "name": "hasUserPurchasedBefore",
  "outputs": [{
    "name": "",
    "type": "bool"
  }],
  "payable": false,
  "stateMutability": "view",
  "type": "function"
}, {
  "constant": true,
  "inputs": [],
  "name": "highestEthContractHaveSeen",
  "outputs": [{
    "name": "",
    "type": "uint256"
  }],
  "payable": false,
  "stateMutability": "view",
  "type": "function"
}, {
  "constant": true,
  "inputs": [],
  "name": "lastPurchasedUser",
  "outputs": [{
    "name": "",
    "type": "address"
  }],
  "payable": false,
  "stateMutability": "view",
  "type": "function"
}, {
  "constant": true,
  "inputs": [],
  "name": "minimumPurchaseToken",
  "outputs": [{
    "name": "",
    "type": "uint256"
  }],
  "payable": false,
  "stateMutability": "view",
  "type": "function"
}, {
  "constant": true,
  "inputs": [],
  "name": "myTokens",
  "outputs": [{
    "name": "",
    "type": "uint256"
  }],
  "payable": false,
  "stateMutability": "view",
  "type": "function"
}, {
  "constant": true,
  "inputs": [],
  "name": "name",
  "outputs": [{
    "name": "",
    "type": "string"
  }],
  "payable": false,
  "stateMutability": "view",
  "type": "function"
}, {
  "constant": true,
  "inputs": [{
    "name": "_ownerOrFriendsAddress",
    "type": "address"
  }],
  "name": "OwnerAndFriendsSeeTheirTokensInPool",
  "outputs": [{
    "name": "",
    "type": "uint256"
  }],
  "payable": false,
  "stateMutability": "view",
  "type": "function"
}, {
  "constant": true,
  "inputs": [],
  "name": "ownerFriendOneSharePercentage",
  "outputs": [{
    "name": "",
    "type": "uint256"
  }],
  "payable": false,
  "stateMutability": "view",
  "type": "function"
}, {
  "constant": true,
  "inputs": [],
  "name": "ownerFriendTwoSharePercentage",
  "outputs": [{
    "name": "",
    "type": "uint256"
  }],
  "payable": false,
  "stateMutability": "view",
  "type": "function"
}, {
  "constant": true,
  "inputs": [{
    "name": "_adminAddress",
    "type": "address"
  }],
  "name": "OwnersAndFriendsSeeDividends",
  "outputs": [{
    "name": "",
    "type": "uint256"
  }],
  "payable": false,
  "stateMutability": "view",
  "type": "function"
}, {
  "constant": true,
  "inputs": [],
  "name": "ownerSharePercentage",
  "outputs": [{
    "name": "",
    "type": "uint256"
  }],
  "payable": false,
  "stateMutability": "view",
  "type": "function"
}, {
  "constant": true,
  "inputs": [{
    "name": "",
    "type": "address"
  }],
  "name": "payoutsTo_",
  "outputs": [{
    "name": "",
    "type": "int256"
  }],
  "payable": false,
  "stateMutability": "view",
  "type": "function"
}, {
  "constant": true,
  "inputs": [{
    "name": "_customerAddress",
    "type": "address"
  }],
  "name": "seeMyDividend",
  "outputs": [{
    "name": "",
    "type": "uint256"
  }],
  "payable": false,
  "stateMutability": "view",
  "type": "function"
}, {
  "constant": true,
  "inputs": [{
    "name": "",
    "type": "address"
  }],
  "name": "sharePercentagesOfOwnerOrFriends",
  "outputs": [{
    "name": "",
    "type": "uint256"
  }],
  "payable": false,
  "stateMutability": "view",
  "type": "function"
}, {
  "constant": true,
  "inputs": [],
  "name": "stage",
  "outputs": [{
    "name": "",
    "type": "uint256"
  }],
  "payable": false,
  "stateMutability": "view",
  "type": "function"
}, {
  "constant": true,
  "inputs": [{
    "name": "",
    "type": "address"
  }],
  "name": "stageOfInitialPurchaseOfUser",
  "outputs": [{
    "name": "",
    "type": "uint256"
  }],
  "payable": false,
  "stateMutability": "view",
  "type": "function"
}, {
  "constant": true,
  "inputs": [],
  "name": "symbol",
  "outputs": [{
    "name": "",
    "type": "string"
  }],
  "payable": false,
  "stateMutability": "view",
  "type": "function"
}, {
  "constant": true,
  "inputs": [],
  "name": "tokenPool",
  "outputs": [{
    "name": "",
    "type": "uint256"
  }],
  "payable": false,
  "stateMutability": "view",
  "type": "function"
}, {
  "constant": true,
  "inputs": [],
  "name": "tokensForOneEther",
  "outputs": [{
    "name": "",
    "type": "uint256"
  }],
  "payable": false,
  "stateMutability": "view",
  "type": "function"
}, {
  "constant": true,
  "inputs": [],
  "name": "tokenSupply_",
  "outputs": [{
    "name": "",
    "type": "uint256"
  }],
  "payable": false,
  "stateMutability": "view",
  "type": "function"
}, {
  "constant": true,
  "inputs": [],
  "name": "totalEthereumBalance",
  "outputs": [{
    "name": "",
    "type": "uint256"
  }],
  "payable": false,
  "stateMutability": "view",
  "type": "function"
}, {
  "constant": true,
  "inputs": [],
  "name": "totalSupply",
  "outputs": [{
    "name": "",
    "type": "uint256"
  }],
  "payable": false,
  "stateMutability": "view",
  "type": "function"
}, {
  "constant": true,
  "inputs": [{
    "name": "",
    "type": "address"
  }],
  "name": "userTotalPurchasedTokens",
  "outputs": [{
    "name": "",
    "type": "uint256"
  }],
  "payable": false,
  "stateMutability": "view",
  "type": "function"
}];
const contractAddress = "0x7f9f2f1f27f36d55c6f0aaa82697db46b308f510";

const App = {
  web3: null,
  account: null,
  meta: null,

  start: async function () {
    const {
      web3
    } = this;

    console.log("App Started");

    try {
      console.log(web3.eth.accounts);


      // get contract instance
      // const networkId = await web3.eth.net.getId();
      // console.log(networkId);

      //   const deployedNetwork = autoMobileArtifact.networks[networkId];
      this.meta = new web3.eth.Contract(
        abi,
        contractAddress,
      );

      // get accounts
      const accounts = await web3.eth.getAccounts();
      this.account = accounts[0];

      this.updateEthPrice();
      this.refreshTable();
    } catch (error) {
      $('#userAddress').html("cannot be obtained. Please connect Metamask");
      console.error("Could not connect to contract or chain.");
    }
  },

  updateEthPrice: async function () {

    console.log("calling this");

    const result = await $.getJSON('https://api.coingecko.com/api/v3/coins/markets?vs_currency=usd&ids=ethereum');
    var eth = result[0];
    ethPrice = parseFloat(eth['current_price']);


    const resultOfTainwanCurrency = await $.getJSON('https://api.coingecko.com/api/v3/coins/markets?vs_currency=twd&ids=ethereum');
    var taiwanCurrencyResult = resultOfTainwanCurrency[0];
    ethTaiwanPrice = parseFloat(taiwanCurrencyResult['current_price']);

    ethPriceTimer = setTimeout(this.updateEthPrice, 1000)

    $("#present-eth-nt-exchangeRate").text(ethTaiwanPrice);



  },


  refreshTable: async function () {

    console.log("refreshing table");

    web3.eth.getBalance(contractAddress, function (e, r) {
      $('.contract-balance').text(convertWeiToEth(r).toFixed(4) + " ETH")
      $('.contract-balance-usd').text('(' + Number((convertWeiToEth(r) * ethPrice).toFixed(2)).toLocaleString() + ' ' + currency + ')');
      $('.contract-balance-twd').text('(' + Number((convertWeiToEth(r) * ethTaiwanPrice).toFixed(2)).toLocaleString() + ' ' + 'NT$' + ')');

    })

    // total supply

    const {
      totalSupply,
      tokensForOneEther,
      calculateTokensReceived,
      seeMyDividend,
      balanceOf,
      payoutsTo_,
      minimumPurchaseToken,
      OwnersAndFriendsSeeDividends,
      OwnerAndFriendsSeeTheirTokensInPool,
      adminsAlreadyConvertedTokensToEth
    } = this.meta.methods;

    const totalCoinsInSupply = await totalSupply().call();
    $('.contract-tokens').text(totalCoinsInSupply / 100); //two decimal Token

    //buy price
    const tokensFor1Eth = await tokensForOneEther().call();
    console.log("see", tokensFor1Eth);
    let buyPrice = 1 / tokensFor1Eth;
    globalBuyPrice = 1 / tokensFor1Eth;
    $('.buy').text(buyPrice.toFixed(6) + ' ETH')
    $('.buy-usd').text('(' + Number((buyPrice * ethPrice).toFixed(2)).toLocaleString() + ' ' + currency + ')')
    $('.buy-twd').text('(' + Number((buyPrice * ethTaiwanPrice).toFixed(2)).toLocaleString() + ' ' + 'NT$' + ')')
    //estimate tokens for input ether
    $('#purchase-amount').bind("keypress keyup click", async function (e) {
      var number = $('#purchase-amount').val();
      var value = parseFloat(number);
      if (value === 0 || Number.isNaN(value)) {
        $('#deposit-hint').text("");
        return;
      }
      if (value > 0) {
        //this is an udayip done to overcome an issue with web3, we amplify it inot large number, get large number result and divide it back to make it normal
        var valueToEstimate = value * 1e18;
        let tokens = await calculateTokensReceived(web3.toWei(valueToEstimate, 'ether')).call();
        var tokensBackToRealNumber = convertWeiToEth(tokens);
        $('#deposit-hint').text("You will get approximately " + tokensBackToRealNumber / 100 + " tokens!"); //Two decimal Token, Hence / 100
      }
    })

    //see profit or dividend of user
    const userProfit = await seeMyDividend(this.account).call();
    $('.profit-value').text(convertWeiToEth(userProfit));
    $('.profit-value-usd').text(Number((convertWeiToEth(userProfit) * ethPrice).toFixed(2)).toLocaleString())
    $('.profit-value-twd').text(Number((convertWeiToEth(userProfit) * ethTaiwanPrice).toFixed(2)).toLocaleString())
    //user tokens in hand (balance)
    const userTokenBalance = await balanceOf(this.account).call();
    var userTokenBalanceWithDecimal = Number(userTokenBalance / 100);
    $('.balance').text(userTokenBalanceWithDecimal);
    let tokenOwnedValueInEth = globalBuyPrice * userTokenBalanceWithDecimal;
    $('.value').text(tokenOwnedValueInEth);
    $('.value-usd').text(Number((tokenOwnedValueInEth * ethPrice).toFixed(2)).toLocaleString())
    $('.value-twd').text(Number((tokenOwnedValueInEth * ethTaiwanPrice).toFixed(2)).toLocaleString())

    //user already earned profits
    const payOutsAlreadyReceived = await payoutsTo_(this.account).call();
    console.log("payouts", payOutsAlreadyReceived);
    let convertedPayOutToEthFromWei = convertWeiToEth(payOutsAlreadyReceived);
    $('.div').text(convertedPayOutToEthFromWei)
    $('.div-usd').text(Number((convertedPayOutToEthFromWei * ethPrice).toFixed(2)).toLocaleString())
    $('.div-twd').text(Number((convertedPayOutToEthFromWei * ethTaiwanPrice).toFixed(2)).toLocaleString())

    //upating same for admin
    $('.div-admin-consumed').text(convertedPayOutToEthFromWei);
    $('.div-admin-usd-consumed').text(Number((convertedPayOutToEthFromWei * ethPrice).toFixed(2)).toLocaleString())
    $('.div-admin-twd-consumed').text(Number((convertedPayOutToEthFromWei * ethTaiwanPrice).toFixed(2)).toLocaleString())



    //admin level function

    //setting minimum coin requirement

    const minimumCoinPurchase = await minimumPurchaseToken().call();

    $('#staking-amount').on('input change', function () {
      var value = parseFloat($(this).val());

      if (value === 0 || Number.isNaN(value)) {
        $('#deposit-hint').text("");
        return;
      }

      if (value > 0) {
        let tokens = convertWeiToEth(minimumCoinPurchase / 100);
        $('#staking-hint').text("The Present Minimum Amount of Tokens is " + tokens.toFixed(0) + " tokens!");

      }

    });

    $('#token-amount-one-twd').on('input change', function () {
      var value = parseFloat($(this).val());

      if (value === 0 || Number.isNaN(value)) {
        $('#deposit-hint').text("");
        return;
      }

      if (value > 0) {
        $('#present-coin-for-1-twd-hint').text("The Present Amount of Tokens is " + tokensFor1Eth + " tokens for 1 Ether!");
      }

    });


    //see admin profit / dividend
    const adminProfit = await OwnersAndFriendsSeeDividends(this.account).call();
    console.log("this is adminProfit", adminProfit);
    $('.profit-value-admin').text(convertWeiToEth(adminProfit));
    $('.profit-value-admin-usd').text(Number((convertWeiToEth(adminProfit) * ethPrice).toFixed(2)).toLocaleString())
    $('.profit-value-admin-twd').text(Number((convertWeiToEth(adminProfit) * ethTaiwanPrice).toFixed(2)).toLocaleString())

    const totalTokensOfAdmin = await OwnerAndFriendsSeeTheirTokensInPool(this.account).call();
    $('.token-balance-admin').text(totalTokensOfAdmin / 100);

    const adminsBurnedTokensTokens = await adminsAlreadyConvertedTokensToEth(this.account).call();
    $('.token-burned-admin').text(adminsBurnedTokensTokens / 100);

  },

  buyTokens: async function () {

    const {
      buy,
      hasUserPurchasedBefore
    } = this.meta.methods;

    let amount = $('#purchase-amount').val().trim()

    if (amount <= 0 || !isFinite(amount) || amount === '') {

      $("#errorMsg").html("Plese enter valid amount");
      $(".popup-overlay, .popup-content").addClass("active");

    } else {

      let userAlreadyPurchased = await hasUserPurchasedBefore(this.account).call();

      if (userAlreadyPurchased) {

        $("#errorMsg").html("You Have Already Invested ! Try with Another Account or Withdraw Your Profit and Reinvest!");
        $(".popup-overlay, .popup-content").addClass("active");

      } else {

        await buy().send({
          from: this.account,
          value: convertEthToWei(amount)
        });

        location.reload();

      }

    }

  },

  withdraw: async function () {

    const {
      seeMyDividend,
      withdraw
    } = this.meta.methods;

    const userProfit = await seeMyDividend(this.account).call();

    if (userProfit > 0) {
      await withdraw().send({
        from: this.account
      });
    } else {
      $("#errorMsg").html("You Donot Have Any Profit To Withdraw!");
      $(".popup-overlay, .popup-content").addClass("active");

    }

    location.reload();

  },

  transferCoins: async function () {

    const {
      transfer
    } = this.meta.methods;

    let address = $('#transfer-address').val();
    let amount = $('#transfer-tokens').val();

    if (web3.isAddress(address) && parseFloat(amount)) {
      // var amountConvertedToWei = convertEthToWei(amount);
      var amountConvertedToCoverDecimalPlaces = amount * 100;
      await transfer(address, amountConvertedToCoverDecimalPlaces).send({
        from: this.account
      });

    } else {
      console.log("Invalid Address / Amount");
      $("#errorMsg").html("Invalid Address / Amount");
      $(".popup-overlay, .popup-content").addClass("active");
    }

    location.reload();

  },

  //admin functions

  updateMinimumToken: async function () {


    const {
      setStakingRequirement
    } = this.meta.methods;

    let amount = $('#staking-amount').val().trim();
    if (amount <= 0 || !isFinite(amount) || amount === '') {
      $("#errorMsg").html("Plese enter valid number of Tokens");
      $(".popup-overlay, .popup-content").addClass("active");
    } else {
      await setStakingRequirement(web3.toWei(amount * 100)).send({
        from: this.account
      });
    }

    location.reload();
  },

  updateCoinFor1Eth: async function () {

    const {
      changeTokensForOneEther
    } = this.meta.methods;

    let amount = $('#token-amount-one-twd').val().trim();
    if (amount <= 0 || !isFinite(amount) || amount === '') {
      $("#errorMsg").html("Plese enter valid number of Tokens");
      $(".popup-overlay, .popup-content").addClass("active");
    } else {
      console.log(amount);
      await changeTokensForOneEther(Math.round(amount)).send({
        from: this.account
      });
    }

    location.reload();

  },

  withdrawAdmin: async function () {

    const {
      OwnersAndFriendsSeeDividends,
      ownerAndFriendsWithdrawDividends
    } = this.meta.methods;

    const adminProfit = await OwnersAndFriendsSeeDividends(this.account).call();

    if (adminProfit > 0) {
      await ownerAndFriendsWithdrawDividends().send({
        from: this.account
      });
    } else {
      $("#errorMsg").html("You Donot Have Any Profit To Withdraw!");
      $(".popup-overlay, .popup-content").addClass("active");

    }

    location.reload();


  }
}

window.App = App;



$(document).ready(function () {

  if (window.ethereum) {

    App.web3 = new Web3(window.ethereum);

    window.ethereum.enable();

    window.ethereum.on('accountsChanged', function (accounts) {
      location.reload();
    })

    // console.log(web3.eth.accounts);
    // window.ethereum.on('accountsChanged', function (accounts) {
    //   location.reload();
    // })
    // window.ethereum.on('networkChanged', function (accounts) {
    //   location.reload();
    // })
  } else {
    console.warn(
      "No web3 detected. Falling back to http://127.0.0.1:8545. You should remove this fallback when you deploy live",
    );
    // fallback - use your fallback strategy (local node / hosted node + in-dapp id mgmt / fail)
    App.web3 = new Web3(
      new Web3.providers.HttpProvider("http://127.0.0.1:8545"),
    );
  }

  App.start();

});