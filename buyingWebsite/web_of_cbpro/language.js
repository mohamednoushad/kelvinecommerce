let arrLang;


$('.translate').click(function(){
    let lang = $(this).attr('id');
    localStorage.setItem('language', lang);
    changeLanguage(lang);
});


function changeLanguage(lang){
    $('.lang').each(function(index,element){
        $(this).html(arrLang[lang][$(this).attr('key')]);
    }); 
}


$(function() {
  

    let pageLang =localStorage.getItem('language');
    let lang = pageLang ? pageLang : "ch";

    function loadJSON(callback) {   

            var xobj = new XMLHttpRequest();
                xobj.overrideMimeType("application/json");
            xobj.open('GET', 'language.json', true); 
            xobj.onreadystatechange = function () {
                if (xobj.readyState == 4 && xobj.status == "200") {
                    callback(xobj.responseText);
                }
            };
            xobj.send(null);  
         }

     loadJSON(function(response) {
        arrLang = JSON.parse(response);
        changeLanguage(lang);
       });

  
})