var dataTimer = null;
let currentAddress;
var dividendValue = 0;
var tokenBalance = 0;
let actual_JSON;
var ethPrice = 0;
var ethPriceTimer = null;
var currency = (typeof default_currency === 'undefined') ? 'USD' : default_currency;
var buyPrice = 0;
var globalBuyPrice = 0;
let contractAddress;
let contract;
var sellPrice = 0;
var infoTimer = null;
let masterNodeLink;



function convertWeiToEth (e) {
    return e / 1e18
  }


function convertEthToWei (e) {
    return 1e18 * e
  }




function loadJSON(callback) {   

    var xobj = new XMLHttpRequest();
        xobj.overrideMimeType("application/json");
    xobj.open('GET', 'cbproToken.json', true); // Replace 'my_data' with the path to your file
    xobj.onreadystatechange = function () {
          if (xobj.readyState == 4 && xobj.status == "200") {
            // Required use of an anonymous callback as .open will NOT return a value but simply returns undefined in asynchronous mode
            callback(xobj.responseText);
          }
    };
    xobj.send(null);  
 }

 function updateEthPrice () {
    clearTimeout(ethPriceTimer)
    if( currency === 'EPY' ){
      ethPrice = 1 / (sellPrice + ((buyPrice - sellPrice) / 2))
      ethPriceTimer = setTimeout(updateEthPrice, 10000)
    } else {
      $.getJSON('https://api.coinmarketcap.com/v1/ticker/ethereum/?convert=' + currency, function (result) {
        var eth = result[0]
        ethPrice = parseFloat(eth['price_' + currency.toLowerCase()])
        ethPriceTimer = setTimeout(updateEthPrice, 10000)
      })
    }
  }

  function getCookie(name) {
    var dc = document.cookie;
    var prefix = name + "=";
    var begin = dc.indexOf("; " + prefix);

if (begin == -1) {
        begin = dc.indexOf(prefix);
        if (begin != 0) return null;
    }
    else
    {
        begin += 2;
        var end = document.cookie.indexOf(";", begin);
        if (end == -1) {
        end = dc.length;
      }
    }

return decodeURI(dc.substring(begin + prefix.length, end));
}



  function whenAvailable(name, callback) {
    var interval = 6000;
       window.setTimeout(function() {
                if (window[name]) {
                    callback(window[name]);
             } else {
                    window.setTimeout(arguments.callee, interval);
             }
    }, interval);
}

 


function updateData() {
    // clearTimeout(dataTimer)

    web3.eth.getBalance(contractAddress, function (e, r) {
    $('.contract-balance').text(convertWeiToEth(r).toFixed(4) + " ETH")
    $('.contract-balance-usd').text('(' + Number((convertWeiToEth(r) * ethPrice).toFixed(2)).toLocaleString() + ' ' + currency + ')');
  })

  contract.totalSupply(function (e, r) {
    let actualSupply = r / 1e18;
    $('.contract-tokens').text(Number(actualSupply.toFixed(0)).toLocaleString());
  })

  contract.buyPrice(function (e, r) {
    let buyPrice = convertWeiToEth(r)
    globalBuyPrice = convertWeiToEth(r)
    $('.buy').text(buyPrice.toFixed(6) + ' ETH')
    $('.buy-usd').text('(' + Number((buyPrice * ethPrice).toFixed(2)).toLocaleString() + ' ' + currency + ')')
  })

  contract.sellPrice(function (e, r) {
    let sellPrice = convertWeiToEth(r)
    $('.sell').text(sellPrice.toFixed(6) + ' ETH')
    $('.sell-usd').text('(' + Number((sellPrice * ethPrice).toFixed(2)).toLocaleString() + ' ' + currency + ')')
  })

  if(currentAddress){
  contract.balanceOf(currentAddress, function (e, r) {
    const tokenAmount = (r / 1e18 * 0.9999)
    $('.balance').text(Number(tokenAmount.toFixed(2)).toLocaleString())
    contract.calculateEthereumReceived(r, function (e, r) {
      let bal = convertWeiToEth(r)
      $('.value').text(bal.toFixed(4))
      $('.value-usd').text(Number((convertWeiToEth(r * 1) * ethPrice).toFixed(2)).toLocaleString())
       if (tokenBalance !== 0) {
            if (bal > tokenBalance) {
            $('.value').addClass('up').removeClass('down')
            setTimeout(function () {
              $('.value').removeClass('up')
            }, 3000)
          }
          else if (bal < tokenBalance) {
            $('.value').addClass('down').removeClass('up')
            setTimeout(function () {
              $('.value').removeClass('down')
            }, 3000)
          }
        }
      tokenBalance = bal
      })
    })
  }

  contract.myDividends(true, function (e, r) {
    let div = convertWeiToEth(r).toFixed(6)

    $('.div').text(div)
    $('input.div').val(div + " ETH")
    $('.div-usd').text(Number((convertWeiToEth(r) * ethPrice).toFixed(2)).toLocaleString())

    if (dividendValue != div) {
      $('.div').fadeTo(100, 0.3, function () { $(this).fadeTo(250, 1.0) })

      dividendValue = div
    }

    contract.myDividends(false, function (e, r) {
      let div = convertWeiToEth(r).toFixed(6)
      let refdiv = (dividendValue - div).toFixed(6);
  
       $('.refdiv').text(refdiv)
       $('.refdiv-usd').text(Number((refdiv * ethPrice).toFixed(2)).toLocaleString())
  
       $('.nonrefdiv').text(div)
       $('.nonrefdiv-usd').text(Number((convertWeiToEth(r) * ethPrice).toFixed(2)).toLocaleString())
    })
  })

  contract.stakingRequirement(function(e,r){
    $('#tokenRequirement').text(convertWeiToEth(r));
   })

   contract.dividendFee_(function(e,r){
     let divDisplayValue = r;

  //    switch (r.toNumber()) {
  //     case 10:
  //         divDisplayValue = 10;
  //         break;
  //     case 5:
  //         divDisplayValue = 20;
  //         break;
  //     case 4:
  //         divDisplayValue = 25;
  //         break;
  //     case 3:
  //         divDisplayValue = 33;
  //         break;
  //      default:
  //         break;
  // }
    $('#divPercentage').text(divDisplayValue);
    $('#divPercentageSecond').text(divDisplayValue);
    $('#divPercentageThird').text(divDisplayValue);
    $('#divPercentageFour').text(divDisplayValue);
    $('#divPercentageFive').text(divDisplayValue);
    $('#div-hint').text("Present Divident Percentage is "+divDisplayValue);
      
   })

   contract.onlyAmbassadors(function (e, r) {
     let div;
 
     if(r){
      div = 'Enabled';
     }else if(!r){
      div = 'Disabled';
     }

     $('input.amb').val("Ambassador Phase is "+ div)
  
  })

  contract.refPercentage(function (e, r) 
      {
          let percentage= r;
          $('#masterNode-hint').text("The Present Masternode Percentage is " + percentage.toFixed(0) + " !");
          $('#masterNodeExchange-hint').text(percentage.toFixed(0));
      })



}

$('#purchase-amount').bind("keypress keyup click", function (e) {
    var number = $('#purchase-amount').val();
    var numTokens = number / globalBuyPrice;
    $('.number-of-tokens').text("With " + (number==0 ? 0 : number) + " ETH you can buy " + numTokens.toFixed(3) + " Tokens");
  })

$('#purchase-amount').on('input change', function() {
    var value = parseFloat($(this).val());


    if ( value === 0 || Number.isNaN(value) ) {
        $('#deposit-hint').text("");
        return;
    }

    if ( value > 0) {
        contract.calculateTokensReceived( convertEthToWei(value), function (e, r) 
        {
            let tokens= convertWeiToEth(r)
            $('#deposit-hint').text("You will get approximately " + tokens.toFixed(0) + " tokens!");
        })
    }
    
});


$('#sell-tokens-amount').on('input change', function() {
  var value = parseFloat($(this).val());


  if ( value === 0 || Number.isNaN(value) ) {
      $('#sell-hint').text("");
      return;
  }

  if ( value > 0) {
      contract.calculateEthereumReceived( convertEthToWei(value), function (e, r) 
      {
          let eth= convertWeiToEth(r);
          $('#sell-hint').text("You will get approximately " + eth.toFixed(2) + " eth!");
      })
  }
  
});

//appends an "active" class to .popup and .popup-content when the "Open" button is clicked
// $(".open").on("click", function(){
//   $(".popup-overlay, .popup-content").addClass("active");
// });

//removes the "active" class to .popup and .popup-content when the "Close" button is clicked 
$(".close, .popup-overlay").on("click", function(){
  $(".popup-overlay, .popup-content").removeClass("active");
});

$('#buy-tokens').click(function () {
let amount = $('#purchase-amount').val().trim()
  if (amount <= 0 || !isFinite(amount) || amount === '') {
    $("#errorMsg").html("Plese enter valid amount");
    $(".popup-overlay, .popup-content").addClass("active");
  } else {
    if(window.location.href.indexOf("masternode")> -1) {
      var url = new URL(window.location.href);
      var refferalAddress = url.searchParams.get("masternode");
      buyTokensWithRefferalLink(refferalAddress,convertEthToWei(amount));
    }else{
      buyTokensWithoutRefferalLink(convertEthToWei(amount));
    }
  }
});

// Reinvest click handler
$('#reinvest-btn').click(function () {
  bundleReInvest();
})

// Withdraw click handler
$('#withdraw-btn').click(function () {
  bundleWithdraw()
})

$('#sell-tokens-btn').click(function () {
  bundleSell(convertEthToWei($("#sell-tokens-amount").val()))
})

$('#transfer-tokens-btn').click(function() {
  let address = $('#transfer-address').val();
  let amount = $('#transfer-tokens').val();

  if (web3.isAddress(address) && parseFloat(amount) ) {
    var amountConverted = convertEthToWei(amount);
    bundleTransferTokens(address,amountConverted);
  }else{
    console("Invalid Address / Amount");
  }
 

});

function openEtherscan() {
    window.open("https://etherscan.io/address/"+contractAddress,"_blank");
 }

function openMasterNodeLink() {
  window.open("https://"+masterNodeLink,"_blank");
}

$('#staking-amount').on('input change', function() {
  var value = parseFloat($(this).val());

  if ( value === 0 || Number.isNaN(value) ) {
      $('#deposit-hint').text("");
      return;
  }

  if ( value > 0) {
      contract.stakingRequirement(function (e, r) 
      {
          let tokens= convertWeiToEth(r)
          $('#staking-hint').text("The Present Staking is " + tokens.toFixed(0) + " tokens!");
      })
  }
  
});

$('#set-staking').click(function () {
  let amount = $('#staking-amount').val().trim()
    if (amount <= 0 || !isFinite(amount) || amount === '') {
      $("#errorMsg").html("Plese enter valid number of Tokens");
      $(".popup-overlay, .popup-content").addClass("active");
    } else {
       setStaking(amount);
    }
  });

  $('#disableAdmin-btn').click(function () {
        disableInitial();
    });

function setDivPercentage() {
  event.preventDefault();
  let selectedValue = $("#divPercentages option:selected").val();
  let inputPercentage = parseInt(selectedValue);
  if((inputPercentage/10)<10 && Number.isInteger(inputPercentage) && Number.isInteger(inputPercentage/10)){
      setDividentPercentage(inputPercentage);

  }
}


function setMasterNode(){
  event.preventDefault();
  let selectedValue = $("#percentage option:selected").val();
  let inputPercentage = parseInt(selectedValue);

  if((inputPercentage/10)<10 && Number.isInteger(inputPercentage) && Number.isInteger(inputPercentage/10)){
  
    updateMasternodePercentage(inputPercentage);

  }

}




$( document ).ready(function() {
    // clearTimeout(dataTimer);
    if (typeof web3 !== 'undefined') {
      console.warn("Using web3 detected from external source like Metamask")
      window.web3 = new Web3(web3.currentProvider);
    } else {
      console.warn("No web3 detected. Falling back to http://localhost:8545. You should remove this fallback when you deploy live, as it's inherently insecure. Consider switching to Metamask for development. More info here: http://truffleframework.com/tutorials/truffle-and-metamask");
      window.web3 = new Web3(new Web3.providers.HttpProvider("http://localhost:8545"));
  
    }

  

    currentAddress = web3.eth.accounts[0];
  
  
    loadJSON(function(response) {
          contractJson = JSON.parse(response);
          contractAddress = contractJson.networks[1].address;
          var abi = contractJson.abi;
          var contractClass = web3.eth.contract(abi)
          contract = contractClass.at(contractAddress)
       });

    updateEthPrice (); 

    whenAvailable("web3", function(){
      if (web3.eth.accounts[0] !== null) {
            masterNodeLink = window.location.hostname+"/exchange.html?masternode=" + currentAddress;
            var element = "<br/><a>"+masterNodeLink +"</a>";
           $("#quoteDisplay").append(element);
    
      } 

      
      
  });

  dataTimer = setTimeout(function () {
    updateData()
  }, 2000)

  setInterval(updateData,8000);
  
    })
